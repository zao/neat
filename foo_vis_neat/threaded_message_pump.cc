#include "threaded_message_pump.h"

#include <chrono>
#include <future>
#include <mutex>
#include <thread>
#include <vector>

struct ThreadedMessagePumpImpl : ThreadedMessagePump {
	explicit ThreadedMessagePumpImpl(service_ptr_t<ThreadedWindow> window);
	~ThreadedMessagePumpImpl();

	void ThreadMain();

	struct ThreadState {
		std::promise<void> startup;
		std::promise<void> terminate;
		service_ptr_t<ThreadedWindow> window;
	} state;
	std::mutex pumpMutex;
	std::unique_ptr<std::thread> pumpThread;
};

service_ptr_t<ThreadedMessagePump> ThreadedMessagePump::Create(service_ptr_t<ThreadedWindow> window) {
	return new service_impl_t<ThreadedMessagePumpImpl>(window);
}

ThreadedMessagePumpImpl::ThreadedMessagePumpImpl(service_ptr_t<ThreadedWindow> window) {
	state.window = std::move(window);
	pumpThread.reset(new std::thread([&]{
		ThreadMain();
	}));
	state.startup.get_future().get();
}

ThreadedMessagePumpImpl::~ThreadedMessagePumpImpl() {
	state.terminate.set_value();
	pumpThread->join();
	pumpThread.reset();
}

void ThreadedMessagePumpImpl::ThreadMain() {
	state.startup.set_value();
	auto terminate = state.terminate.get_future();
	while (terminate.wait_for(std::chrono::seconds(0)) != std::future_status::ready) {
		state.window->Sim();
		state.window->Vis();
	}
}