add_library(foo_vis_neat SHARED
"grid_fluid.cc"
"grid_fluid.h"
"neat_main.cc"
"shader_builder.cc"
"shader_builder.h"
"threaded_message_pump.cc"
"threaded_message_pump.h"
"threaded_window.cc"
"threaded_window.h"
"vape_window.cc"
"vape_window.h"
)
target_link_libraries(foo_vis_neat component)
set_target_properties(foo_vis_neat
PROPERTIES
RUNTIME_OUTPUT_DIRECTORY_DEBUG "${COMPONENTS_DIR}/foo_vis_neat"
RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL "${COMPONENTS_DIR}/foo_vis_neat"
RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO "${COMPONENTS_DIR}/foo_vis_neat"
RUNTIME_OUTPUT_DIRECTORY_RELEASE "${COMPONENTS_DIR}/foo_vis_neat"
)
