#pragma once

#include <d3d11.h>
#include <atlbase.h>
#include <atlcom.h>

#include <string>
#include <memory>

namespace util {
	struct ShaderBuilder : ID3DInclude {
		ID3D11Device* device;
		std::string assetRoot;

		explicit ShaderBuilder(ID3D11Device* device);

		STDMETHOD(Open)(THIS_ D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) override;
		STDMETHOD(Close)(THIS_ LPCVOID pData);

		void CompileVertexShader(char const* filename, CComPtr<ID3D11VertexShader>& out, CComPtr<ID3DBlob>* signature = nullptr);
		void CompilePixelShader(char const* filename, CComPtr<ID3D11PixelShader>& out);
		void CompileGeometryShader(char const* filename, CComPtr<ID3D11GeometryShader>& out);
		void CompileComputeShader(char const* filename, CComPtr<ID3D11ComputeShader>& out);
		void CompileHullShader(char const* filename, CComPtr<ID3D11HullShader>& out);
		void CompileDomainShader(char const* filename, CComPtr<ID3D11DomainShader>& out);
	};
}