#include <type_traits>

namespace util {
	struct Zeroes {
		template <typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value>>
		operator T* () const {
			static T ts[64]{};
			return ts;
		}
	};

	struct Nulls {
		template <typename I>
		operator I* const* () const {
			static I* is[64]{};
			return is;
		}
	};
}