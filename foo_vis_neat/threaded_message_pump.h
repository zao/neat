#pragma once
#include <SDK/foobar2000.h>
#include "threaded_window.h"

struct ThreadedMessagePump : service_base {
	static service_ptr_t<ThreadedMessagePump> Create(service_ptr_t<ThreadedWindow> window);
	FB2K_MAKE_SERVICE_INTERFACE(ThreadedMessagePump, service_base)
};

// {EB940FAB-F7FF-4015-BAF8-01EB0E0FC885}
__declspec(selectany) GUID const ThreadedMessagePump::class_guid =
{ 0xeb940fab, 0xf7ff, 0x4015, { 0xba, 0xf8, 0x1, 0xeb, 0xe, 0xf, 0xc8, 0x85 } };