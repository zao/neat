#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims; uint pad;
	float dt; float rdx;
}

Texture3D<float4> w_in :  register(t0);
RWTexture3D<float> div_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (OnBoundary(dims, tid)) return;

	float wL = w_in[tid + uint3(-1, 0, 0)].x;
	float wR = w_in[tid + uint3(+1, 0, 0)].x;
	float wB = w_in[tid + uint3(0, -1, 0)].y;
	float wT = w_in[tid + uint3(0, +1, 0)].y;
	float wN = w_in[tid + uint3(0, 0, -1)].z;
	float wF = w_in[tid + uint3(0, 0, +1)].z;
	div_out[tid] = 0.5 * rdx * ((wR - wL) + (wT - wB) + (wF - wN));
}
