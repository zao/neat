#ifndef FLUID_INDEX_HLSL
#define FLUID_INDEX_HLSL

#define OnBoundary(dims, pc) (\
	pc.x == 0 || pc.y == 0 || pc.z == 0 || \
	pc.x == dims.x - 1 || pc.y == dims.y - 1 || pc.z == dims.z - 1)

#endif
