#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims; uint pad;
	float dt; float rdx;
}

cbuffer Position : register(b1) {
	float scale;
}

Texture3D<float> x_in : register(t0);
RWTexture3D<float> x_out : register(u0);

[numthreads(8, 8, 8)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (tid.z == 0) {
		x_out[tid] = x_in[tid + int3(0, 0, +1)] * scale;
	}
	else if (tid.z == dims.z - 1) {
		x_out[tid] = x_in[tid + int3(0, 0, -1)] * scale;
	}
	else if (tid.y == 0) {
		x_out[tid] = x_in[tid + int3(0, +1, 0)] * scale;
	}
	else if (tid.y == dims.y - 1) {
		x_out[tid] = x_in[tid + int3(0, -1, 0)] * scale;
	}
	else if (tid.x == 0) {
		x_out[tid] = x_in[tid + int3(+1, 0, 0)] * scale;
	}
	else if (tid.x == dims.x - 1) {
		x_out[tid] = x_in[tid + int3(-1, 0, 0)] * scale;
	}
	else {
		x_out[tid] = x_in[tid];
	}
}
