struct vs_input {
	uint id : SV_VertexID;
};

struct ps_input {
	float2 tc : Texcoord;
	float4 pos : SV_Position;
};

void main(in vs_input vs, out ps_input ps) {
	float2 pos[3] = {
		float2(-1.0f, 1.0f), float2(3.0f, 1.0f), float2(-1.0f, -3.0f)
	};
	float2 tc[3] = {
		float2(0.0f, 0.0f), float2(2.0f, 0.0f), float2(0.0f, 2.0f)
	};
	ps.pos = float4(pos[vs.id], 0.0, 1.0);
	ps.tc = tc[vs.id];
}