#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims;
}

cbuffer JacobiParams : register(b1) {
	float alpha; float rBeta;
}

Texture3D<float> b_in : register(t0);
Texture3D<float> x_in : register(t1);
RWTexture3D<float> x_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (OnBoundary(dims, tid)) return;

	uint3 idx = tid;
	float xL = x_in[tid + uint3(-1, 0, 0)];
	float xR = x_in[tid + uint3(+1, 0, 0)];
	float xB = x_in[tid + uint3(0, -1, 0)];
	float xT = x_in[tid + uint3(0, +1, 0)];
	float xN = x_in[tid + uint3(0, 0, -1)];
	float xF = x_in[tid + uint3(0, 0, +1)];
	float bC = b_in[idx];
	x_out[idx] = (xL + xR + xB + xT + xN + xF + alpha * bC) * rBeta;
}
