#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims; uint pad;
	float dt; float rdx;
}

cbuffer Position : register(b1) {
	uint3 src; uint pad2;
	float3 dir;
}

Texture3D<float4> u_in : register(t0);
RWTexture3D<float4> u_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (OnBoundary(dims, tid)) return;

	float3 del = int3(tid) - int3(src);
	// del *= dx, but we don't have dx
	del /= rdx;
	float d = length(del);
	float footprint = 0.1;
	if (d < footprint) {
		float mag = pow((footprint - d) / footprint, 2);
		float3 u = u_in[tid].xyz + mag * dir;
		u_out[tid] = float4(u, 1.0);
	}
	else {
		u_out[tid] = u_in[tid];
	}
}
