#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims;
}

RWTexture3D<float4> x_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	uint3 idx = tid;
	x_out[idx] = float4(0.0, 0.0, 0.0, 0.0);
}
