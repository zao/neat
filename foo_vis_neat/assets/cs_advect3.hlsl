#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims; uint pad;
	float dt; float rdx;
}

SamplerState linearClamp : register(s0);

Texture3D<float4> u_in : register(t0);
RWTexture3D<float4> u_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (OnBoundary(dims, tid)) return;

	uint3 idx = tid;
	float3 pc = tid - dt * rdx * u_in[idx].xyz;
	u_out[idx] = u_in.SampleLevel(linearClamp, (pc + 0.5) / dims, 0);
}
