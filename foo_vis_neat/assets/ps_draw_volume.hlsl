#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims;
}

struct ps_input {
	float2 tc : Texcoord;
};

SamplerState linearClamp : register(s0);

Texture3D<float> p_in : register(t0);
Texture3D<float4> u_in : register(t1);
Texture3D<float> ink_in : register(t2);
Texture3D<float> div_in : register(t3);

void main(in ps_input ps, out float4 rs_color : SV_Target) {
	float3 I = float3(0.0, 0.0, 0.0);
	float2 spriteTC = fmod(ps.tc * 2.0, 1.0);
	//for (uint i = dims.z / 2; i == dims.z / 2; ++i) {
#if NEAT_DRAW_FOUR
	for (uint i = 1; i < dims.z - 1; ++i) {
		float3 tc = float3(spriteTC, (i + 0.5) / dims.z);
		if (ps.tc.y < 0.5) {
			// Upper
			if (ps.tc.x < 0.5) {
				// Left
				float ink = ink_in.SampleLevel(linearClamp, tc, 0);
				I.xyz = max(I.xyz, ink);
			}
			else {
				// Right
				float ink = ink_in.SampleLevel(linearClamp, tc, 0);
				I.xyz = I.x + ink * (1.0 - I.x) * (1.0 - i / float(dims.z));
				if (I.x > 1.0) {
					I.xyz = 1.0;
					break;
				}
			}
		}
		else {
			// Lower
			if (ps.tc.x < 0.5) {
				// Left
				float3 u = u_in.SampleLevel(linearClamp, tc, 0).xyz;
				u = u * 0.5 + 0.5;
				I.xyz += u.xyz / dims.z;
			}
			else {
				// Right
				float divergence = div_in.SampleLevel(linearClamp, tc, 0);
				I.xyz = max(I.xyz, divergence);
			}
		}
	}
#else
	for (uint i = 1; i < dims.z - 1; ++i) {
		float3 tc = float3(ps.tc, (i + 0.5) / dims.z);
		// Right
		float ink = ink_in.SampleLevel(linearClamp, tc, 0);
		I.xyz = I.x + ink * (1.0 - I.x) * (1.0 - i / float(dims.z));
		if (I.x > 1.0) {
			I.xyz = 1.0;
			break;
		}
	}
#endif
	rs_color = float4(float3(1.0, 1.0, 1.0) * I, 1.0);
}
