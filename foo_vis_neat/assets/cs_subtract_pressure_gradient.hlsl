#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims; uint pad;
	float dt; float rdx;
}

Texture3D<float> p_in : register(t0);
Texture3D<float4> u_in : register(t1);
RWTexture3D<float4> u_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (OnBoundary(dims, tid)) return;

	uint3 idx = tid;
	float pL = p_in[tid + uint3(-1, 0, 0)];
	float pR = p_in[tid + uint3(+1, 0, 0)];
	float pB = p_in[tid + uint3(0, -1, 0)];
	float pT = p_in[tid + uint3(0, +1, 0)];
	float pN = p_in[tid + uint3(0, 0, -1)];
	float pF = p_in[tid + uint3(0, 0, +1)];
	float3 u = u_in[idx].xyz - 0.5 * rdx * float3(pR - pL, pT - pB, pF - pN);
	u_out[idx] = float4(u, 1.0);
}
