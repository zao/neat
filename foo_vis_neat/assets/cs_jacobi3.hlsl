#include "fluid_index.hlsl"

cbuffer Dimensions : register(b0) {
	uint3 dims;
}

cbuffer JacobiParams : register(b1) {
	float alpha; float rBeta;
}

Texture3D<float4> b_in : register(t0);
Texture3D<float4> x_in : register(t1);
RWTexture3D<float4> x_out : register(u0);

[numthreads(16, 16, 1)]
void main(uint3 tid : SV_DispatchThreadID) {
	if (OnBoundary(dims, tid)) return;

	uint3 idx = tid;
	float3 xL = x_in[tid + uint3(-1, 0, 0)].xyz;
	float3 xR = x_in[tid + uint3(+1, 0, 0)].xyz;
	float3 xB = x_in[tid + uint3(0, -1, 0)].xyz;
	float3 xT = x_in[tid + uint3(0, +1, 0)].xyz;
	float3 xN = x_in[tid + uint3(0, 0, -1)].xyz;
	float3 xF = x_in[tid + uint3(0, 0, +1)].xyz;
	float3 bC = b_in[idx].xyz;
	float3 x = (xL + xR + xB + xT + xN + xF + alpha * bC) * rBeta;
	x_out[idx] = float4(x, 1.0);
}
