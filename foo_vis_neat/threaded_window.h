#pragma once
#include <SDK/foobar2000.h>

struct ThreadedWindow : ui_element_instance {
	virtual void Sim() = 0;
	virtual void Vis() = 0;
	FB2K_MAKE_SERVICE_INTERFACE(ThreadedWindow, ui_element_instance)
};

// {EECFD4F8-C768-495F-8AA9-E1A5EDEE7435}
__declspec(selectany) GUID const ThreadedWindow::class_guid =
{ 0xeecfd4f8, 0xc768, 0x495f, { 0x8a, 0xa9, 0xe1, 0xa5, 0xed, 0xee, 0x74, 0x35 } };