#include "vape_window.h"

#include "nils.h"
#include "threaded_message_pump.h"

#include <d3dcompiler.h>

#include <sstream>

GUID VapeWindow::g_get_guid() {
	return s_guid;
}

GUID VapeWindow::g_get_subclass() {
	return ui_element_subclass_playback_visualisation;
}

void VapeWindow::g_get_name(pfc::string_base& out) {
	out = "Neat Vape";
}

char const* VapeWindow::g_get_description() {
	return "A fancy smoke visualization that reacts to music.";
}

ui_element_config::ptr VapeWindow::g_get_default_configuration() {
	auto cfg = ui_element_config::g_create_empty(s_guid);
	return cfg;
}

void VapeWindow::set_configuration(ui_element_config::ptr data) {
}

ui_element_config::ptr VapeWindow::get_configuration() {
	return g_get_default_configuration();
}

void VapeWindow::initialize_window(HWND parent) {
	Create(parent, nullptr, nullptr, WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 0);
	this->pump = ThreadedMessagePump::Create(this);
}

void VapeWindow::notify(GUID const& what, t_size param1, void const* param2, t_size param2Size) {
}

VapeWindow::VapeWindow(ui_element_config::ptr cfg, ui_element_instance_callback::ptr callback)
: callback(callback)
{}

VapeWindow::~VapeWindow() {}

bool VapeWindow::edit_mode_context_menu_test(POINT const& p_point, bool p_fromkeyboard) {
	return true;
}

double Now() {
	int64_t freq;
	int64_t t;
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&freq));
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&t));
	return t / static_cast<double>(freq);
}

LRESULT VapeWindow::OnWM_CREATE(CREATESTRUCT* cs) {
	DWORD createFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#define NEAT_DEBUG_DIRECT3D 0
#if _DEBUG || NEAT_DEBUG_DIRECT3D
	createFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	DXGI_SWAP_CHAIN_DESC scd{};
	scd.BufferCount = 2;
	scd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	scd.BufferDesc.Height = cs->cy ? cs->cy : 8;
	scd.BufferDesc.Width = cs->cx ? cs->cx : 8;
	scd.BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = *this;
	scd.SampleDesc.Count = 1;
	scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scd.Windowed = TRUE;
	D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createFlags, nullptr, 0, D3D11_SDK_VERSION, &scd,
		&resources.swapChain, &resources.device, &resources.featureLevel, &resources.ctx);

	builder = std::make_unique<util::ShaderBuilder>(resources.device);

	builder->CompileVertexShader("vs_full_tri.hlsl", resources.fullTriVS);
	builder->CompilePixelShader("ps_draw_volume.hlsl", resources.drawVolumePS);

	fluid = std::make_unique<GridFluid>(resources.device, resources.ctx, 128, 128, 16);
	lastTime = Now();
	lastPerfTime = lastTime;
	static_api_ptr_t<visualisation_manager> vm;
	vm->create_stream(vis, visualisation_manager::KStreamFlagNewFFT);
	return TRUE;
}

void VapeWindow::OnWM_DESTROY() {
	this->pump = {};
}

void VapeWindow::OnWM_MOUSEMOVE(UINT wparam, CPoint point) {
	if (mouseHeld) {
		mouseX = static_cast<float>(point.x) / resources.lastClientWidth;
		mouseY = static_cast<float>(point.y) / resources.lastClientHeight;
	}
}

void VapeWindow::OnWM_LBUTTONDOWN(UINT wparam, CPoint point) {
	mouseHeld = true;
	OnWM_MOUSEMOVE(wparam, point);
}

void VapeWindow::OnWM_LBUTTONUP(UINT wparam, CPoint point) {
	mouseHeld = false;
}

void VapeWindow::OnWM_RBUTTONUP(UINT wparam, CPoint point) {
	if (callback->is_edit_mode_enabled()) {
		SetMsgHandled(FALSE);
		return;
	}
}

LRESULT VapeWindow::OnWM_ERAGEBKGND(HDC dc) {
	t_ui_color bgColor = callback->query_std_color(ui_color_background);
	SetDCBrushColor(dc, bgColor);
	RECT clientRect{};
	GetClientRect(&clientRect);
	FillRect(dc, &clientRect, static_cast<HBRUSH>(GetStockObject(DC_BRUSH)));
	return 1;
}

// {C512A8BC-F33A-4728-AD80-380038156FC9}
GUID const VapeWindow::s_guid =
{ 0xc512a8bc, 0xf33a, 0x4728, { 0xad, 0x80, 0x38, 0x0, 0x38, 0x15, 0x6f, 0xc9 } };

static service_factory_t<ui_element_impl<VapeWindow>> g_vape_window;

void VapeWindow::Sim() {
	double dt = fluid->dt;
	double now = Now();
	int framesSimulated{};
	audio_chunk_impl chunk;
	double playTime{};
	bool playing = vis->get_absolute_time(playTime);
	bool gotData = vis->get_spectrum_absolute(chunk, playTime, 1024);
	while (now >= lastTime + dt) {
		float const RM = static_cast<float>(RAND_MAX);
		auto& ctx = resources.ctx;
		if (playing && gotData) {
			float pos[] = { 0.5f, 0.5f, 0.5f };
			pos[2] = chunk.get_data()[0];
			fluid->AddForce(pos);
		}
		else {
			float pos[] = { 0.5f, 0.5f, 0.5f };
			fluid->AddForce(pos);
		}
		if (mouseHeld) {
			float pos[] = { fmodf(mouseX*2.0f, 1.0f), fmodf(mouseY*2.0f, 1.0f), 0.5f };
			fluid->AddForce(pos);
		}
		fluid->Sim();
		lastTime += dt;
		++framesSimulated;
		++perfCount;
		if (framesSimulated == 4) {
			lastTime = now;
		}
	}
	if (lastPerfTime + 0.75f < lastTime && perfCount) {
		std::ostringstream oss;
		oss << "Frame time: " << (1000.0f * (lastTime - lastPerfTime) / perfCount) << "ms";
		console::info(oss.str().c_str());
		lastPerfTime = lastTime;
		perfCount = 0;
	}
}

void XbgrToColor(t_ui_color xbgr, float out[4]) {
	out[0] = (xbgr>>0  & 0xFF) / 255.0f;
	out[1] = (xbgr>>8  & 0xFF) / 255.0f;
	out[2] = (xbgr>>16 & 0xFF) / 255.0f;
	out[3] = (xbgr>>24 & 0xFF) / 255.0f;
}

void VapeWindow::Vis() {
	HRESULT hr{S_OK};
	{
		RECT clientRect{};
		GetClientRect(&clientRect);
		if (clientRect.right != resources.lastClientWidth || clientRect.bottom != resources.lastClientHeight) {
			resources.lastClientWidth = clientRect.right;
			resources.lastClientHeight = clientRect.bottom;
			if (resources.lastClientWidth && resources.lastClientHeight) {
				resources.sized = {};
				hr = resources.swapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
				CComPtr<ID3D11Texture2D> backbuffer;
				hr = resources.swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backbuffer));
				hr = resources.device->CreateRenderTargetView(backbuffer, nullptr, &resources.sized.backbufferRTV);
				resources.sized.viewport.MaxDepth = 1.0f;
				resources.sized.viewport.Height = static_cast<float>(resources.lastClientHeight);
				resources.sized.viewport.Width = static_cast<float>(resources.lastClientWidth);
			}
		}
	}
	if (resources.lastClientWidth == 0 || resources.lastClientHeight == 0)
		return;

	t_ui_color bgColor = callback->query_std_color(ui_color_background);
	float clearColor[4];
	XbgrToColor(bgColor, clearColor);
	resources.ctx->ClearRenderTargetView(resources.sized.backbufferRTV, clearColor);

	// Draw volume
	{
		auto& ctx = resources.ctx;
		ctx->RSSetViewports(1, &resources.sized.viewport);
		ctx->OMSetRenderTargets(1, &resources.sized.backbufferRTV.p, nullptr);

		ID3D11ShaderResourceView* psSRVs[] = { fluid->PressureBufferSource(), fluid->VelocityBufferSource(), fluid->InkBufferSource(), fluid->divergenceSRV };
		ID3D11Buffer* psCBs[] = { fluid->dimensionCB };
		ID3D11SamplerState* psSamplers[] = { fluid->linearClampSampler };

		ctx->VSSetShader(resources.fullTriVS, nullptr, 0);
		ctx->PSSetShader(resources.drawVolumePS, nullptr, 0);
		ctx->PSSetShaderResources(0, 4, psSRVs);
		ctx->PSSetConstantBuffers(0, 1, psCBs);
		ctx->PSSetSamplers(0, 1, psSamplers);
		
		ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		ctx->Draw(3, 0);

		ctx->VSSetShader(nullptr, nullptr, 0);
		ctx->PSSetShader(nullptr, nullptr, 0);
		ctx->PSSetShaderResources(0, 4, util::Nulls{});
		ctx->PSSetConstantBuffers(0, 1, util::Nulls{});
		ctx->PSSetSamplers(0, 1, util::Nulls{});
	}

	resources.swapChain->Present(1, 0);
}
