#include "shader_builder.h"
#include <SDK/foobar2000.h>
#include <d3d11.h>
#include <d3dcompiler.h>

namespace util {
#if NEAT_SKIP_OPTIMIZATION
	DWORD const flags1 = D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	DWORD const flags1 = D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

	ShaderBuilder::ShaderBuilder(ID3D11Device* device)
		: device(device)
	{
		std::string path = core_api::get_my_full_path();
		assetRoot = path.substr(0, path.find_last_of('\\') + 1);
	}

	COM_DECLSPEC_NOTHROW HRESULT STDMETHODCALLTYPE ShaderBuilder::Open
	(THIS_ D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) {
		std::unique_ptr<char[]> buffer;
		std::string filename = assetRoot + pFileName;
		service_ptr_t<file> f;
		abort_callback_dummy cb;
		t_filesize sz{};
		try {
			filesystem::g_open_read(f, filename.c_str(), cb);
			sz = f->get_size(cb);
			buffer.reset(new char[static_cast<size_t>(sz)]);
			f->read(buffer.get(), static_cast<size_t>(sz), cb);
		}
		catch (pfc::exception e) {
			return E_FAIL;
		}
		*ppData = buffer.release();
		*pBytes = static_cast<UINT>(sz);
		return S_OK;
	}

	COM_DECLSPEC_NOTHROW HRESULT STDMETHODCALLTYPE ShaderBuilder::Close
	(THIS_ LPCVOID pData) {
		auto buffer = static_cast<char*>(const_cast<void*>(pData));
		delete [] buffer;
		return S_OK;
	}

	template <typename F>
	void ReadBuildRun(ID3DInclude* includes, std::string filename, char const* profile, F fun) {
		HRESULT hr{S_OK};
		while (1) {
			pfc::array_t<char> sink;
			{
				service_ptr_t<file> f;
				abort_callback_dummy cb;
				try {
					filesystem::g_open_read(f, filename.c_str(), cb);
					if (f->get_size(cb) == 0) {
						DebugBreak();
						continue;
					}
					f->read_till_eof(sink, cb);
				}
				catch (pfc::exception e) {
					DebugBreak();
					continue;
				}
			}
			CComPtr<ID3DBlob> code, messages;
			hr = D3DCompile(sink.get_ptr(), sink.get_count(), filename.c_str(), nullptr, includes, "main", profile, flags1, 0, &code, &messages);
			if (messages && messages->GetBufferSize()) {
				pfc::string8 msg(static_cast<char const*>(messages->GetBufferPointer()), messages->GetBufferSize());
				OutputDebugStringA(msg.get_ptr());
				console::info(msg.get_ptr());
			}
			if (SUCCEEDED(hr)) {
				if (fun(code)) {
					break;
				}
			}
			DebugBreak();
		}
	}

	void ShaderBuilder::CompileVertexShader(char const* filename, CComPtr<ID3D11VertexShader>& out, CComPtr<ID3DBlob>* signature) {
		ReadBuildRun(this, assetRoot + filename, "vs_5_0", [&](ID3DBlob* code) {
			HRESULT hr{S_OK};
			CComPtr<ID3D11VertexShader> shader;
			hr = device->CreateVertexShader(code->GetBufferPointer(), code->GetBufferSize(), nullptr, &shader);
			if (SUCCEEDED(hr)) {
				out = std::move(shader);
				if (signature) {
					*signature = code;
				}
				return true;
			}
			return false;
		});
	}

	void ShaderBuilder::CompilePixelShader(char const* filename, CComPtr<ID3D11PixelShader>& out) {
		ReadBuildRun(this, assetRoot + filename, "ps_5_0", [&](ID3DBlob* code) {
			HRESULT hr{S_OK};
			CComPtr<ID3D11PixelShader> shader;
			hr = device->CreatePixelShader(code->GetBufferPointer(), code->GetBufferSize(), nullptr, &shader);
			if (SUCCEEDED(hr)) {
				out = std::move(shader);
				return true;
			}
			return false;
		});
	}

	void ShaderBuilder::CompileGeometryShader(char const* filename, CComPtr<ID3D11GeometryShader>& out) {
		ReadBuildRun(this, assetRoot + filename, "gs_5_0", [&](ID3DBlob* code) {
			HRESULT hr{S_OK};
			CComPtr<ID3D11GeometryShader> shader;
			hr = device->CreateGeometryShader(code->GetBufferPointer(), code->GetBufferSize(), nullptr, &shader);
			if (SUCCEEDED(hr)) {
				out = std::move(shader);
				return true;
			}
			return false;
		});
	}

	void ShaderBuilder::CompileComputeShader(char const* filename, CComPtr<ID3D11ComputeShader>& out) {
		ReadBuildRun(this, assetRoot + filename, "cs_5_0", [&](ID3DBlob* code) {
			HRESULT hr{S_OK};
			CComPtr<ID3D11ComputeShader> shader;
			hr = device->CreateComputeShader(code->GetBufferPointer(), code->GetBufferSize(), nullptr, &shader);
			if (SUCCEEDED(hr)) {
				out = std::move(shader);
				return true;
			}
			return false;
		});
	}

	void ShaderBuilder::CompileHullShader(char const* filename, CComPtr<ID3D11HullShader>& out) {
		ReadBuildRun(this, assetRoot + filename, "hs_5_0", [&](ID3DBlob* code) {
			HRESULT hr{S_OK};
			CComPtr<ID3D11HullShader> shader;
			hr = device->CreateHullShader(code->GetBufferPointer(), code->GetBufferSize(), nullptr, &shader);
			if (SUCCEEDED(hr)) {
				out = std::move(shader);
				return true;
			}
			return false;
		});
	}

	void ShaderBuilder::CompileDomainShader(char const* filename, CComPtr<ID3D11DomainShader>& out) {
		ReadBuildRun(this, assetRoot + filename, "ds_5_0", [&](ID3DBlob* code) {
			HRESULT hr{S_OK};
			CComPtr<ID3D11DomainShader> shader;
			hr = device->CreateDomainShader(code->GetBufferPointer(), code->GetBufferSize(), nullptr, &shader);
			if (SUCCEEDED(hr)) {
				out = std::move(shader);
				return true;
			}
			return false;
		});
	}
}