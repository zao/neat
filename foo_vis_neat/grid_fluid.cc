#include "grid_fluid.h"

#include "nils.h"
#include "shader_builder.h"
#include <fstream>
#include <memory>
#include <vector>

#include <d3d11_1.h>

size_t GridFluid::CellCount() const {
	return cellCols * cellRows * cellSlices;
}

ID3D11ShaderResourceView* GridFluid::VelocityBufferSource() const { return velocitySRVs[currentVelocityBuffer]; }
ID3D11UnorderedAccessView* GridFluid::VelocityBufferTarget() const { return velocityUAVs[1 - currentVelocityBuffer]; }
ID3D11UnorderedAccessView* GridFluid::VelocityBufferInplace() const { return velocityUAVs[currentVelocityBuffer]; }

ID3D11ShaderResourceView* GridFluid::PressureBufferSource() const { return pressureSRVs[currentPressureBuffer]; }
ID3D11UnorderedAccessView* GridFluid::PressureBufferTarget() const { return pressureUAVs[1 - currentPressureBuffer]; }
ID3D11UnorderedAccessView* GridFluid::PressureBufferInplace() const { return pressureUAVs[currentPressureBuffer]; }

ID3D11ShaderResourceView* GridFluid::InkBufferSource() const { return inkSRVs[currentInkBuffer]; }
ID3D11UnorderedAccessView* GridFluid::InkBufferTarget() const { return inkUAVs[1 - currentInkBuffer]; }
ID3D11UnorderedAccessView* GridFluid::InkBufferInplace() const { return inkUAVs[currentInkBuffer]; }

void GridFluid::SwapVelocityBuffers() { currentVelocityBuffer = 1 - currentVelocityBuffer; }
void GridFluid::SwapPressureBuffers() { currentPressureBuffer = 1 - currentPressureBuffer; }
void GridFluid::SwapInkBuffers() { currentInkBuffer = 1 - currentInkBuffer; }

GridFluid::GridFluid(ID3D11Device* device, ID3D11DeviceContext* ctx, uint16_t cellCols, uint16_t cellRows, uint16_t cellSlices)
	: device(device), ctx(ctx), cellCols(cellCols), cellRows(cellRows), cellSlices(cellSlices)
{
	auto builder = std::make_unique<util::ShaderBuilder>(device);
	builder->CompileComputeShader("cs_clear1.hlsl", clear1CS);
	builder->CompileComputeShader("cs_clear3.hlsl", clear3CS);
	builder->CompileComputeShader("cs_advect1.hlsl", advect1CS);
	builder->CompileComputeShader("cs_advect3.hlsl", advect3CS);
	builder->CompileComputeShader("cs_jacobi1.hlsl", jacobi1CS);
	builder->CompileComputeShader("cs_jacobi3.hlsl", jacobi3CS);
	builder->CompileComputeShader("cs_add_quantity1.hlsl", addQuantity1CS);
	builder->CompileComputeShader("cs_add_quantity3.hlsl", addQuantity3CS);
	builder->CompileComputeShader("cs_compute_divergence.hlsl", computeDivergenceCS);
	builder->CompileComputeShader("cs_subtract_pressure_gradient.hlsl", subtractPressureGradientCS);
	builder->CompileComputeShader("cs_propagate_boundary1.hlsl", propagateBoundary1CS);
	builder->CompileComputeShader("cs_propagate_boundary3.hlsl", propagateBoundary3CS);
	
	dx = 2.0f / cellCols;
	dt = 1.0f / 120.0f;
	viscosity = 0.00003f;
	diffuseIterations = 20;
	pressureIterations = 80;

	HRESULT hr{S_OK};
	{
		CD3D11_SAMPLER_DESC sd{CD3D11_DEFAULT{}};
		hr = device->CreateSamplerState(&sd, &linearClampSampler);
	}
	{
		struct Dimensions {
			uint32_t dims[3]; uint32_t pad1;
			float dt; float rdx; float pad2[2];
		};
		size_t elementCount = cellCols * cellRows * cellSlices;
		{
			D3D11_BUFFER_DESC bd{};
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.ByteWidth = sizeof(Dimensions);
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;
			bd.StructureByteStride = 0;
			bd.Usage = D3D11_USAGE_IMMUTABLE;
			D3D11_SUBRESOURCE_DATA srd{};
			Dimensions dimensions{};
			dimensions.dims[0] = cellCols;
			dimensions.dims[1] = cellRows;
			dimensions.dims[2] = cellSlices;
			dimensions.dt = dt;
			dimensions.rdx = 1.0f / dx;
			srd.pSysMem = &dimensions;
			hr = device->CreateBuffer(&bd, &srd, &dimensionCB);
			std::string name("Dimension");
			hr = dimensionCB->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
		}
		{
			D3D11_BUFFER_DESC bd{};
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.ByteWidth = sizeof(float[4]);
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;
			bd.StructureByteStride = 0;
			bd.Usage = D3D11_USAGE_IMMUTABLE;
			D3D11_SUBRESOURCE_DATA srd{};
			float alpha = (dx*dx) / (viscosity * dt);
			float rBeta = 1.0f / (6 + alpha);
			float jacobi[4] = { alpha, rBeta };
			srd.pSysMem = jacobi;
			hr = device->CreateBuffer(&bd, &srd, &diffuseJacobiCB);
			std::string name("DiffuseJacobi");
			hr = diffuseJacobiCB->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());

			alpha = -(dx*dx);
			rBeta = 1.0f / 6;
			jacobi[0] = alpha;
			jacobi[1] = rBeta;
			hr = device->CreateBuffer(&bd, &srd, &pressureJacobiCB);
			name = "PressureJacobi";
			hr = pressureJacobiCB->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
		}
		{
			D3D11_BUFFER_DESC bd{};
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.ByteWidth = sizeof(float[4]);
			bd.Usage = D3D11_USAGE_IMMUTABLE;
			float data[4]{};
			D3D11_SUBRESOURCE_DATA srd{};
			srd.pSysMem = data;
			data[0] = -1.0f;
			hr = device->CreateBuffer(&bd, &srd, &boundaryMirrorCB);
			std::string name("Mirror boundary");
			hr = boundaryMirrorCB->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			
			data[0] = 1.0f;
			hr = device->CreateBuffer(&bd, &srd, &boundaryReplicateCB);
			name = "Replicate boundary";
			hr = boundaryReplicateCB->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			
			data[0] = 0.0f;
			hr = device->CreateBuffer(&bd, &srd, &boundaryZeroCB);
			name = "Zero boundary";
			hr = boundaryZeroCB->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());

		}
		{
			D3D11_TEXTURE3D_DESC td{};
			td.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
			td.Depth = cellSlices;
			td.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			td.Height = cellRows;
			td.MipLevels = 1;
			td.Usage = D3D11_USAGE_DEFAULT;
			td.Width = cellCols;

			std::vector<float> velInit(elementCount * 4);
			D3D11_SUBRESOURCE_DATA srd{};
			srd.pSysMem = velInit.data();
			srd.SysMemPitch = sizeof(float[4]) * cellCols;
			srd.SysMemSlicePitch = srd.SysMemPitch * cellRows;
			for (size_t i = 0; i < 2; ++i) {
				hr = device->CreateTexture3D(&td, &srd, &velocity[i]);
				hr = device->CreateShaderResourceView(velocity[i], nullptr, &velocitySRVs[i]);
				hr = device->CreateUnorderedAccessView(velocity[i], nullptr, &velocityUAVs[i]);
				char const* names[] = { "Velocity A", "Velocity B" };
				std::string name = names[i];
				hr = velocity[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
				hr = velocitySRVs[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
				hr = velocityUAVs[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			}
		}
		{
			D3D11_TEXTURE3D_DESC td{};
			td.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
			td.Depth = cellSlices;
			td.Format = DXGI_FORMAT_R32_FLOAT;
			td.Height = cellRows;
			td.MipLevels = 1;
			td.Usage = D3D11_USAGE_DEFAULT;
			td.Width = cellCols;

			for (size_t i = 0; i < 2; ++i) {
				hr = device->CreateTexture3D(&td, nullptr, &pressure[i]);
				hr = device->CreateShaderResourceView(pressure[i], nullptr, &pressureSRVs[i]);
				hr = device->CreateUnorderedAccessView(pressure[i], nullptr, &pressureUAVs[i]);
				char const* names[] = { "Pressure A", "Pressure B" };
				std::string name = names[i];
				hr = pressure[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
				hr = pressureSRVs[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
				hr = pressureUAVs[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			}

			hr = device->CreateTexture3D(&td, nullptr, &divergence);
			hr = device->CreateShaderResourceView(divergence, nullptr, &divergenceSRV);
			hr = device->CreateUnorderedAccessView(divergence, nullptr, &divergenceUAV); 
			std::string name("Divergence");
			hr = divergence->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			hr = divergenceSRV->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			hr = divergenceUAV->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());

			std::vector<float> inkInit(elementCount);
			D3D11_SUBRESOURCE_DATA srd{};
			srd.pSysMem = inkInit.data();
			srd.SysMemPitch = sizeof(float) * cellCols;
			srd.SysMemSlicePitch = srd.SysMemPitch * cellRows;
			for (size_t i = 0; i < 2; ++i) {
				hr = device->CreateTexture3D(&td, &srd, &ink[i]);
				hr = device->CreateShaderResourceView(ink[i], nullptr, &inkSRVs[i]);
				hr = device->CreateUnorderedAccessView(ink[i], nullptr, &inkUAVs[i]);
				char const* names[] = { "Ink A", "Ink B" };
				std::string name = names[i];
				hr = ink[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
				hr = inkSRVs[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
				hr = inkUAVs[i]->SetPrivateData(WKPDID_D3DDebugObjectName, name.size(), name.data());
			}
		}
	}
}

void GridFluid::AddForce(float* pos) {
	std::array<float, 3> p{pos[0], pos[1], pos[2]};
	std::unique_lock<std::mutex> lk(manipulateMutex);
	clicks.push_back(std::move(p));
}

struct ComputeOperationStorage {
	ComputeOperationStorage(size_t ceil)
		: data(new char[ceil])
		, ceil(ceil)
	{}

	~ComputeOperationStorage() {
		if (next != 0) { abort(); }
	}

	void* Allocate(size_t amount) {
		if (amount > ceil - next) {
			abort();
		}
		void* ret = data.get() + next;
		next += amount;
		total += amount;
		hwm = (std::max)(hwm, next);
		return ret;
	}

	template <typename T>
	T* Allocate(size_t count) {
		auto ret = static_cast<T*>(Allocate(count * sizeof(T)));
		return ret;
	}

	void Return(size_t amount) {
		if (amount > next) {
			abort();
		}
		next -= amount;
	}

private:
	std::unique_ptr<char[]> data;
	size_t next{};
	size_t ceil;

	// debug
	size_t total{};
	size_t hwm{};
};

struct ComputeOperation {
	ComputeOperationStorage& storage;
	ID3D11Device* device;
	ID3D11DeviceContext* ctx;
	ID3D11ComputeShader* cs;
	uint8_t cbCount{}, samplerCount{}, srvCount{}, uavCount{};
	ID3D11Buffer** cbs{};
	ID3D11SamplerState** samplers{};
	ID3D11ShaderResourceView** srvs{};
	ID3D11UnorderedAccessView** uavs{};

	ComputeOperation(ComputeOperationStorage& storage, ID3D11Device* device, ID3D11DeviceContext* ctx, ID3D11ComputeShader* cs)
		: storage(storage), device(device), ctx(ctx), cs(cs)
	{}

	~ComputeOperation() {
		storage.Return(
			cbCount * sizeof(ID3D11Buffer*) +
			samplerCount * sizeof(ID3D11SamplerState*) +
			srvCount * sizeof(ID3D11ShaderResourceView*) +
			uavCount * sizeof(ID3D11UnorderedAccessView*));
	}

	template <typename V, typename X>
	void Copy(V* vs, X const& x) {
		*vs = x;
	}

	template <typename V, typename X, typename... Xs>
	void Copy(V* vs, X const& x, Xs const&... xs) {
		Copy(vs, x);
		Copy(++vs, xs...);
	}

	template <typename... CBs>
	ComputeOperation& SetCBs(CBs... cbs) {
		if (this->cbs) { abort(); }
		cbCount = static_cast<uint8_t>(sizeof...(CBs));
		this->cbs = storage.Allocate<ID3D11Buffer*>(cbCount);
		Copy(this->cbs, cbs...);
		return *this;
	}
	
	template <typename... Samplers>
	ComputeOperation& SetSamplers(Samplers... samplers) {
		if (this->samplers) { abort(); }
		samplerCount = static_cast<uint8_t>(sizeof...(Samplers));
		this->samplers = storage.Allocate<ID3D11SamplerState*>(samplerCount);
		Copy(this->samplers, samplers...);
		return *this;
	}

	template <typename... SRVs>
	ComputeOperation& SetSRVs(SRVs... srvs) {
		if (this->srvs) { abort(); }
		srvCount = static_cast<uint8_t>(sizeof...(SRVs));
		this->srvs = storage.Allocate<ID3D11ShaderResourceView*>(srvCount);
		Copy(this->srvs, srvs...);
		return *this;
	}

	template <typename... UAVs>
	ComputeOperation& SetUAVs(UAVs... uavs) {
		if (this->uavs) { abort(); }
		uavCount = static_cast<uint8_t>(sizeof...(UAVs));
		this->uavs = storage.Allocate<ID3D11UnorderedAccessView*>(uavCount);
		Copy(this->uavs, uavs...);
		return *this;
	}

	ComputeOperation& Invoke(size_t x, size_t y, size_t z) {
		ctx->CSSetShader(cs, nullptr, 0);
		ctx->CSSetConstantBuffers(0, cbCount, cbs);
		ctx->CSSetSamplers(0, samplerCount, samplers);
		ctx->CSSetShaderResources(0, srvCount, srvs);
		ctx->CSSetUnorderedAccessViews(0, uavCount, uavs, util::Zeroes{});

		ctx->Dispatch(x, y, z);

		ctx->CSSetShader(nullptr, nullptr, 0);
		ctx->CSSetConstantBuffers(0, cbCount, util::Nulls{});
		ctx->CSSetSamplers(0, samplerCount, util::Nulls{});
		ctx->CSSetShaderResources(0, srvCount, util::Nulls{});
		ctx->CSSetUnorderedAccessViews(0, uavCount, util::Nulls{}, util::Zeroes{});
		return *this;
	}
};

#define NEAT_SIMULATE_INK 1
void GridFluid::Sim() {
	HRESULT hr{S_OK};
	CComPtr<ID3DUserDefinedAnnotation> uda;
	hr = ctx->QueryInterface(IID_PPV_ARGS(&uda));
	ComputeOperationStorage storage(1<<9);

	uda->BeginEvent(L"Fluid sim");

	// Mirror velocity boundaries
	{
		uda->BeginEvent(L"Mirror velocity boundary");
		ComputeOperation(storage, device, ctx, propagateBoundary3CS)
			.SetCBs(dimensionCB, boundaryMirrorCB)
			.SetSRVs(VelocityBufferSource())
			.SetUAVs(VelocityBufferTarget())
			.Invoke(cellCols / 8, cellRows / 8, cellSlices / 8);
		SwapVelocityBuffers();
		uda->EndEvent();
	}

	// u = advect(u);
	{
		uda->BeginEvent(L"Advect velocity");
		ComputeOperation(storage, device, ctx, advect3CS)
			.SetCBs(dimensionCB)
			.SetSamplers(linearClampSampler)
			.SetSRVs(VelocityBufferSource())
			.SetUAVs(VelocityBufferTarget())
			.Invoke(cellCols / 16, cellRows / 16, cellSlices);
		SwapVelocityBuffers();
		uda->EndEvent();
	}

#if NEAT_SIMULATE_INK
	// Zero ink boundaries
	{
		uda->BeginEvent(L"Zero ink boundary");
		ComputeOperation(storage, device, ctx, propagateBoundary1CS)
			.SetCBs(dimensionCB, boundaryZeroCB)
			.SetSRVs(InkBufferSource())
			.SetUAVs(InkBufferTarget())
			.Invoke(cellCols / 8, cellRows / 8, cellSlices / 8);
		SwapInkBuffers();
		uda->EndEvent();
	}

	// ink = advect(ink);
	{
		uda->BeginEvent(L"Advect ink");
		ComputeOperation(storage, device, ctx, advect1CS)
			.SetCBs(dimensionCB)
			.SetSamplers(linearClampSampler)
			.SetSRVs(VelocityBufferSource(), InkBufferSource())
			.SetUAVs(InkBufferTarget())
			.Invoke(cellCols / 16, cellRows / 16, cellSlices);
		SwapInkBuffers();
		uda->EndEvent();
	}
#endif

	// u = addForces(u);
	{
		std::deque<std::array<float, 3>> clicks;
		{
			std::unique_lock<std::mutex> lk(manipulateMutex);
			clicks.swap(this->clicks);
		}
		uda->BeginEvent(L"Add forces");
		for (auto click : clicks) {
			CComPtr<ID3D11Buffer> clickCB;
			{
				struct Click {
					uint32_t pos[3]; uint32_t pad1;
					float vel[3]; float rate;
				};
				D3D11_BUFFER_DESC bd{};
				bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				bd.ByteWidth = sizeof(Click);
				bd.CPUAccessFlags = 0;
				bd.MiscFlags = 0;
				bd.StructureByteStride = 0;
				bd.Usage = D3D11_USAGE_IMMUTABLE;
				D3D11_SUBRESOURCE_DATA srd{};
				Click c{};
				c.pos[0] = static_cast<uint32_t>(click[0] * cellCols);
				c.pos[1] = static_cast<uint32_t>(click[1] * cellRows);
				c.pos[2] = static_cast<uint32_t>(click[2] * cellSlices);
				float fx = static_cast<float>(rand() / static_cast<float>(RAND_MAX));
				float fy = static_cast<float>(rand() / static_cast<float>(RAND_MAX));
				float fz = static_cast<float>(rand() / static_cast<float>(RAND_MAX));
				c.vel[0] = 15.0f * 2.0f * (fx - 0.5f);
				c.vel[1] = 15.0f * 2.0f * (fy - 0.5f);
				c.vel[2] = 15.0f * 2.0f * (fz - 0.5f);
				c.rate = 1.0f;
				srd.pSysMem = &c;
				hr = device->CreateBuffer(&bd, &srd, &clickCB);
			}

			{
				ComputeOperation(storage, device, ctx, addQuantity3CS)
					.SetCBs(dimensionCB, clickCB)
					.SetSRVs(VelocityBufferSource())
					.SetUAVs(VelocityBufferTarget())
					.Invoke(cellCols / 16, cellRows / 16, cellSlices);
				SwapVelocityBuffers();
			}

			{
				ComputeOperation(storage, device, ctx, addQuantity1CS)
					.SetCBs(dimensionCB, clickCB)
					.SetSRVs(InkBufferSource())
					.SetUAVs(InkBufferTarget())
					.Invoke(cellCols / 16, cellRows / 16, cellSlices);
				SwapInkBuffers();
			}
		}
		uda->EndEvent();
	}

	// Mirror velocity boundaries
	{
		uda->BeginEvent(L"Mirror velocity boundary");
		ComputeOperation(storage, device, ctx, propagateBoundary3CS)
			.SetCBs(dimensionCB, boundaryMirrorCB)
			.SetSRVs(VelocityBufferSource())
			.SetUAVs(VelocityBufferTarget())
			.Invoke(cellCols / 8, cellRows / 8, cellSlices / 8);
		SwapVelocityBuffers();
		uda->EndEvent();
	}

	// u = diffuse(u);
	{
		uda->BeginEvent(L"Diffusion jacobi");
		for (size_t iteration = 0; iteration < diffuseIterations; ++iteration) {
			ComputeOperation(storage, device, ctx, jacobi3CS)
				.SetCBs(dimensionCB, diffuseJacobiCB)
				.SetSRVs(VelocityBufferSource(), VelocityBufferSource())
				.SetUAVs(VelocityBufferTarget())
				.Invoke(cellCols / 16, cellRows / 16, cellSlices);
			SwapVelocityBuffers();
		}
		uda->EndEvent();
	}

	// p = computePressure(u);
	{
		// Compute divergence
		{
			uda->BeginEvent(L"Compute divergence");
			ComputeOperation(storage, device, ctx, computeDivergenceCS)
				.SetCBs(dimensionCB)
				.SetSRVs(VelocityBufferSource())
				.SetUAVs(divergenceUAV)
				.Invoke(cellCols / 16, cellRows / 16, cellSlices);
			uda->EndEvent();
		}

		// Init pressure to zero
		{
			uda->BeginEvent(L"Clear pressure");
			ComputeOperation(storage, device, ctx, clear1CS)
				.SetCBs(dimensionCB)
				.SetUAVs(PressureBufferInplace())
				.Invoke(cellCols / 16, cellRows / 16, cellSlices);
			uda->EndEvent();
		}

		uda->BeginEvent(L"Pressure jacobi");
		for (size_t iteration = 0; iteration < pressureIterations; ++iteration) {
			// Replicate pressure boundaries
			{
				uda->BeginEvent(L"Replicate pressure boundary");
				ComputeOperation(storage, device, ctx, propagateBoundary1CS)
					.SetCBs(dimensionCB, boundaryReplicateCB)
					.SetSRVs(PressureBufferSource())
					.SetUAVs(PressureBufferTarget())
					.Invoke(cellCols / 8, cellRows / 8, cellSlices / 8);
				SwapPressureBuffers();
				uda->EndEvent();
			}

			ComputeOperation(storage, device, ctx, jacobi1CS)
				.SetCBs(dimensionCB, pressureJacobiCB)
				.SetSRVs(divergenceSRV, PressureBufferSource())
				.SetUAVs(PressureBufferTarget())
				.Invoke(cellCols / 16, cellRows / 16, cellSlices);
			SwapPressureBuffers();
		}
		uda->EndEvent();
	}

	// Mirror velocity boundaries
	{
		uda->BeginEvent(L"Mirror velocity boundary");
		ComputeOperation(storage, device, ctx, propagateBoundary3CS)
			.SetCBs(dimensionCB, boundaryMirrorCB)
			.SetSRVs(VelocityBufferSource())
			.SetUAVs(VelocityBufferTarget())
			.Invoke(cellCols / 8, cellRows / 8, cellSlices / 8);
		SwapVelocityBuffers();
		uda->EndEvent();
	}

	// u = subtractPressureGradient(u, p);
	{
		uda->BeginEvent(L"Subtract pressure gradient");
		ComputeOperation(storage, device, ctx, subtractPressureGradientCS)
			.SetCBs(dimensionCB)
			.SetSRVs(PressureBufferSource(), VelocityBufferSource())
			.SetUAVs(VelocityBufferTarget())
			.Invoke(cellCols / 16, cellRows / 16, cellSlices);
		SwapVelocityBuffers();
		uda->EndEvent();
	}
	uda->EndEvent();
}