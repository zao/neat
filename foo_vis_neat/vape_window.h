#pragma once
#include <ATLHelpers/ATLHelpers.h>
#include "grid_fluid.h"
#include "shader_builder.h"
#include "threaded_message_pump.h"
#include "threaded_window.h"

#include <atlbase.h>
#include <atlapp.h>
#include <atlframe.h>
#include <atlwin.h>

#include <d3d11.h>
#include <dxgi.h>

#include <memory>

struct VapeWindow : ThreadedWindow, CWindowImpl<VapeWindow> {
	DECLARE_WND_CLASS_EX(L"vape-window", CS_HREDRAW | CS_VREDRAW, 0)
	static GUID g_get_guid();
	static GUID g_get_subclass();
	static void g_get_name(pfc::string_base& out);
	static char const* g_get_description();

	static ui_element_config::ptr g_get_default_configuration();
	void set_configuration(ui_element_config::ptr data);
	ui_element_config::ptr get_configuration();

	void initialize_window(HWND parent);

	void notify(GUID const& what, t_size param1, void const* param2, t_size param2Size);
	VapeWindow(ui_element_config::ptr cfg, ui_element_instance_callback::ptr callback);
	~VapeWindow();

	bool edit_mode_context_menu_test(POINT const& p_point, bool p_fromkeyboard) override;

	void Sim() override;
	void Vis() override;

	BEGIN_MSG_MAP(VapeWindow)
		MSG_WM_CREATE(OnWM_CREATE)
		MSG_WM_DESTROY(OnWM_DESTROY)
		MSG_WM_ERASEBKGND(OnWM_ERAGEBKGND)
		MSG_WM_LBUTTONDOWN(OnWM_LBUTTONDOWN)
		MSG_WM_LBUTTONUP(OnWM_LBUTTONUP)
		MSG_WM_RBUTTONUP(OnWM_RBUTTONUP)
		MSG_WM_MOUSEMOVE(OnWM_MOUSEMOVE)
	END_MSG_MAP()

	LRESULT OnWM_CREATE(CREATESTRUCT*);
	void OnWM_DESTROY();
	LRESULT OnWM_ERAGEBKGND(HDC dc);
	void OnWM_LBUTTONDOWN(UINT wparam, CPoint point);
	void OnWM_LBUTTONUP(UINT wparam, CPoint point);
	void OnWM_RBUTTONUP(UINT wparam, CPoint point);
	void OnWM_MOUSEMOVE(UINT wparam, CPoint point);

	service_ptr_t<ThreadedMessagePump> pump;
	bool mouseHeld = false;
	float mouseX{};
	float mouseY{};

	double lastTime{};
	double lastPerfTime{};
	int perfCount{};
	service_ptr_t<visualisation_stream_v3> vis;

	struct Resources {
		size_t lastClientWidth{}, lastClientHeight{};
		CComPtr<IDXGISwapChain> swapChain;
		CComPtr<ID3D11Device> device;
		CComPtr<ID3D11DeviceContext> ctx;
		D3D_FEATURE_LEVEL featureLevel;
		struct Sized {
			CComPtr<ID3D11RenderTargetView> backbufferRTV;
			D3D11_VIEWPORT viewport;
		} sized;

		CComPtr<ID3D11VertexShader> fullTriVS;
		CComPtr<ID3D11PixelShader> drawVolumePS;
	} resources;

	std::unique_ptr<GridFluid> fluid;

	std::unique_ptr<util::ShaderBuilder> builder;

	static GUID const s_guid;
	ui_element_instance_callback::ptr callback;
};