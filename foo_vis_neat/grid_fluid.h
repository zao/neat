#pragma once

#include <d3d11.h>

#include <atlbase.h>
#include <atlcom.h>

#include <stdint.h>

#include <array>
#include <deque>
#include <mutex>

struct GridFluid {
	GridFluid(ID3D11Device* device, ID3D11DeviceContext* ctx, uint16_t cellCols, uint16_t cellRows, uint16_t cellSlices);

	CComPtr<ID3D11Device> device;
	CComPtr<ID3D11DeviceContext> ctx;
	CComPtr<ID3D11Texture3D> velocity[2], divergence, pressure[2], ink[2];
	CComPtr<ID3D11SamplerState> linearClampSampler;
	CComPtr<ID3D11Buffer> dimensionCB;
	CComPtr<ID3D11Buffer> diffuseJacobiCB, pressureJacobiCB;
	CComPtr<ID3D11Buffer> boundaryMirrorCB, boundaryReplicateCB, boundaryZeroCB;

	CComPtr<ID3D11ShaderResourceView> velocitySRVs[2], divergenceSRV, pressureSRVs[2], inkSRVs[2];
	CComPtr<ID3D11UnorderedAccessView> velocityUAVs[2], divergenceUAV, pressureUAVs[2], inkUAVs[2];

	CComPtr<ID3D11ComputeShader> propagateBoundary1CS, propagateBoundary3CS;
	CComPtr<ID3D11ComputeShader> jacobi1CS, jacobi3CS;
	CComPtr<ID3D11ComputeShader> clear1CS, clear3CS;
	CComPtr<ID3D11ComputeShader> advect1CS, advect3CS, addQuantity1CS, addQuantity3CS, computeDivergenceCS, subtractPressureGradientCS;

	size_t cellCols, cellRows, cellSlices;
	uint8_t currentVelocityBuffer = 0;
	uint8_t currentPressureBuffer = 0;
	uint8_t currentInkBuffer = 0;
	float dx, dt;
	float viscosity;
	size_t diffuseIterations;
	size_t pressureIterations;

	std::mutex manipulateMutex;
	std::deque<std::array<float, 3>> clicks;

	void Sim();
	void AddForce(float* pos);

	size_t CellCount() const;

	ID3D11ShaderResourceView* VelocityBufferSource() const;
	ID3D11UnorderedAccessView* VelocityBufferTarget() const;
	ID3D11UnorderedAccessView* VelocityBufferInplace() const;

	ID3D11ShaderResourceView* PressureBufferSource() const;
	ID3D11UnorderedAccessView* PressureBufferTarget() const;
	ID3D11UnorderedAccessView* PressureBufferInplace() const;
	
	ID3D11ShaderResourceView* InkBufferSource() const;
	ID3D11UnorderedAccessView* InkBufferTarget() const;
	ID3D11UnorderedAccessView* InkBufferInplace() const;

	void SwapVelocityBuffers();
	void SwapPressureBuffers();
	void SwapInkBuffers();
};