#include <SDK/foobar2000.h>

DECLARE_COMPONENT_VERSION("Neat Visualizations", "1.0", "zao")
VALIDATE_COMPONENT_FILENAME("foo_vis_neat.dll")

#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"dxguid.lib")